#ifndef _CFL_SOCKET_H_

#define _CFL_SOCKET_H_

#include <stdio.h>

#if defined(__BORLANDC__)
   #include <io.h>
   #include <windows.h>
   #include <winsock.h>
#elif defined(_MSC_VER)
   #include <winsock2.h>
   #include <um\ws2ipdef.h>
   #include <um\ws2tcpip.h>
#elif defined(__MINGW64__)
   #include <winsock2.h>
   #include <ws2ipdef.h>
   #include <ws2tcpip.h>
#else
   #include <sys/socket.h>
   #include <unistd.h>
   #include <netdb.h>
#endif

#include "cfl_types.h"

//struct _CFL_SOCKET {
//   socket     handle;
//   CFL_INT32  errorCode;
//   CFL_STRP   errorMessage;
//   CFL_UINT32 bufferSize;
//   char      *buffer;
//};

extern CFL_SOCKET cfl_socket_open(const char *serverAddress, CFL_UINT16 port);
extern CFL_INT32 cfl_socket_close(CFL_SOCKET socket);
extern CFL_INT32 cfl_socket_send(CFL_SOCKET socket, char *buffer, int len);
extern CFL_BOOL cfl_socket_sendAll(CFL_SOCKET socket, CFL_BUFFERP buffer);
extern CFL_INT32 cfl_socket_receive(CFL_SOCKET socket, char *buffer, int len);
extern CFL_BOOL cfl_socket_receiveAll(CFL_SOCKET socket, CFL_BUFFERP buffer, CFL_UINT32 packetLen, CFL_UINT32 timeout);
extern CFL_INT32 cfl_socket_selectRead(CFL_SOCKET socket, long sec, long mSec);
extern CFL_INT32 cfl_socket_selectWrite(CFL_SOCKET socket, long sec, long mSec);
extern CFL_INT32 cfl_socket_lastErrorCode();
extern CFL_BOOL cfl_socket_setBlockingMode(CFL_SOCKET socket, CFL_BOOL block);

#endif
