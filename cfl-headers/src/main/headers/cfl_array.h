#ifndef CFL_ARRAY_H_

#define CFL_ARRAY_H_

#include "cfl_types.h"

struct _CFL_ARRAY {
   CFL_UINT8  objectType;
   CFL_UINT32 ulLength;
   CFL_UINT32 ulCapacity;
   void       **items;
};

extern CFL_ARRAYP cfl_array_new(CFL_UINT32 ulCapacity);
extern CFL_ARRAYP cfl_array_newLen(CFL_UINT32 ulLen);
extern void cfl_array_free(CFL_ARRAYP array);
extern void cfl_array_add(CFL_ARRAYP array, void *item);
extern void cfl_array_del(CFL_ARRAYP array, CFL_UINT32 ulIndex);
extern void *cfl_array_get(CFL_ARRAYP array, CFL_UINT32 ulIndex);
extern void cfl_array_set(CFL_ARRAYP array, CFL_UINT32 ulIndex, void *item);
extern void cfl_array_clear(CFL_ARRAYP array);
extern CFL_UINT32 cfl_array_length(CFL_ARRAYP array);
extern void cfl_array_setLength(CFL_ARRAYP array, CFL_UINT32 newLen);
extern CFL_ARRAYP cfl_array_clone(CFL_ARRAYP other);
extern void *cfl_array_remove(CFL_ARRAYP array, CFL_UINT32 ulIndex);
extern void *cfl_array_removeLast(CFL_ARRAYP array);

#endif
