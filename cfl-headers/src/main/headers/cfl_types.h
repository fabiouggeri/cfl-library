#ifndef _CFL_TYPES_H_

#define _CFL_TYPES_H_

#if defined(_MSC_VER) || defined(__BORLANDC__) || defined(__MINGW64__)
   #include <windows.h>
#endif

#include "cfl_ints.h"

typedef int8_t        CFL_INT8;
typedef uint8_t       CFL_UINT8;
typedef int16_t       CFL_INT16;
typedef uint16_t      CFL_UINT16;
typedef int32_t       CFL_INT32;
typedef uint32_t      CFL_UINT32;
typedef int64_t       CFL_INT64;
typedef uint64_t      CFL_UINT64;
typedef double        CFL_DOUBLE;
typedef unsigned char CFL_BOOL;

#define CFL_SOCKET               SOCKET
#define CFL_INVALID_SOCKET       INVALID_SOCKET
#define CFL_HANDLE               HANDLE
#define CFL_INVALID_HANDLE_VALUE INVALID_HANDLE_VALUE
#define CFL_SOCKET_ERROR         SOCKET_ERROR

#define CFL_FALSE 0
#define CFL_TRUE  ( ! 0 )

#ifndef NULL
   #define NULL    0
#endif

#define CFL_MAX_INT_FLOAT  16777216

#define CFL_MAX_INT_DOUBLE 9007199254740992

// #define BIT_FIELD : 1
#define BIT_FIELD

#define CFL_TYPE_UNKNOWN     0
#define CFL_TYPE_STRING      1
#define CFL_TYPE_ARRAY       2
#define CFL_TYPE_BITMAP      3
#define CFL_TYPE_HASH        4
#define CFL_TYPE_ERROR       5

#if defined(_MSC_VER) || defined(__BORLANDC__) || defined(__MINGW64__)
   #define CFL_THREAD_HANDLE      HANDLE
   #define CFL_THREAD_ID          DWORD
   #define CFL_SLIM_LOCK_HANDLE   SRWLOCK
   #define CFL_CRITICAL_SECTION   CRITICAL_SECTION
   #define CFL_CONDITION_VARIABLE CONDITION_VARIABLE
#else
   #define CFL_THREAD_HANDLE       int
   #define CFL_THREAD_ID           pthread_t
   #define CFL_SLIM_LOCK_HANDLE 
   #define CFL_CRITICAL_SECTION
   #define CFL_CONDITION_VARIABLE
#endif

struct _CFL_HASH_ENTRY;
typedef struct _CFL_HASH_ENTRY CFL_HASH_ENTRY;
typedef struct _CFL_HASH_ENTRY *CFL_HASH_ENTRYP;

struct _CFL_HASH;
typedef struct _CFL_HASH CFL_HASH;
typedef struct _CFL_HASH *CFL_HASHP;

struct _CFL_ARRAY;
typedef struct _CFL_ARRAY CFL_ARRAY;
typedef struct _CFL_ARRAY *CFL_ARRAYP;

struct _CFL_LLIST;
typedef struct _CFL_LLIST CFL_LLIST;
typedef struct _CFL_LLIST *CFL_LLISTP;

struct _CFL_STR;
typedef struct _CFL_STR CFL_STR;
typedef struct _CFL_STR *CFL_STRP;

struct _CFL_BITMAP;
typedef struct _CFL_BITMAP CFL_BITMAP;
typedef struct _CFL_BITMAP *CFL_BITMAPP;

struct _CFL_LOCK;
typedef struct _CFL_LOCK CFL_LOCK;
typedef struct _CFL_LOCK *CFL_LOCKP;

struct _CFL_ITERATOR;
typedef struct _CFL_ITERATOR CFL_ITERATOR;
typedef struct _CFL_ITERATOR *CFL_ITERATORP;

typedef void (*CFL_ITERATOR_FUNC)(void *);

struct _CFL_ITERATOR_CLASS;
typedef struct _CFL_ITERATOR_CLASS CFL_ITERATOR_CLASS;
typedef struct _CFL_ITERATOR_CLASS *CFL_ITERATOR_CLASSP;

struct _CFL_BTREE_NODE;
typedef struct _CFL_BTREE_NODE CFL_BTREE_NODE;
typedef struct _CFL_BTREE_NODE *CFL_BTREE_NODEP;

typedef CFL_INT16 (* BTREE_CMP_VALUE_FUNC)(void *pValue1, void *pValue2, CFL_BOOL bExact);
typedef void (* BTREE_FREE_KEY_FUNC)(void *pValue);
typedef CFL_BOOL (* BTREE_WALK_CALLBACK)(void *pValue);

struct _CFL_BTREE;
typedef struct _CFL_BTREE CFL_BTREE;
typedef struct _CFL_BTREE *CFL_BTREEP;

struct _CFL_BUFFER;
typedef struct _CFL_BUFFER CFL_BUFFER;
typedef struct _CFL_BUFFER *CFL_BUFFERP;

struct _CFL_DATE;
typedef struct _CFL_DATE CFL_DATE;
typedef struct _CFL_DATE *CFL_DATEP;

#define CFL_NO_ERROR_TYPE 0
#define CFL_NO_ERROR_CODE 0

struct _CFL_ERROR;
typedef struct _CFL_ERROR CFL_ERROR;
typedef struct _CFL_ERROR *CFL_ERRORP;

#endif
