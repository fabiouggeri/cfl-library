#ifndef _CFL_INTS_H

#define _CFL_INTS_H

#if (defined(_MSC_VER) && _MSC_VER < 1600) || (defined(__BORLANDC__) && __BORLANDC__ < 0x0600)
   #define int8_t              signed __int8
   #define int16_t             signed __int16
   #define int32_t             signed __int32
   #define int64_t             signed __int64
   #define uint8_t             unsigned __int8
   #define uint16_t            unsigned __int16
   #define uint32_t            unsigned __int32
   #define uint64_t            unsigned __int64
#else
   #include <stdint.h>
#endif

#endif
