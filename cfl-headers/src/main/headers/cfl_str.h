#ifndef CFL_STR_H_

#define CFL_STR_H_

#include "cfl_types.h"

#define CFL_STR_CLEAR(s) if (s != NULL) { \
                           cfl_str_free(s); \
                           s = NULL; }
#define CFL_STR_APPEND_CONST(s, c) cfl_str_appendLen(s, c, (int) sizeof(c) - 1);
#define CFL_STR_SET_LEN(s, c)      cfl_str_setValueLen(s, c, (int) sizeof(c) - 1);
#define CFL_STR_SET(s,v)           if(s) \
                                      cfl_str_setValue(s,v); \
                                   else \
                                      cfl_str_setValueLen(s=cfl_str_new(strlen(v)+1),v,s->ulCapacity-1)
#define CFL_STR_APPEND(s,v)        if(s) \
                                      cfl_str_append(s,v,NULL); \
                                   else \
                                      cfl_str_appendLen(s=cfl_str_new(strlen(v)+1),v,s->ulCapacity-1)

#define CFL_ISUPPER( c ) ( ( c ) >= 'A' && ( c ) <= 'Z' )
#define CFL_ISLOWER( c ) ( ( c ) >= 'a' && ( c ) <= 'z' )
#define CFL_ISDIGIT( c ) ( ( c ) >= '0' && ( c ) <= '9' )
#define CFL_TOUPPER( c ) ( ( c ) - ( 'a' - 'A' ) )
#define CFL_TOLOWER( c ) ( ( c ) + ( 'a' - 'A' ) )

#define CFL_STR_EMPTY { CFL_TYPE_STRING, 0, 1, 0, "" }

struct _CFL_STR {
   CFL_UINT8  objectType;
   CFL_UINT32 ulLength;
   CFL_UINT32 ulCapacity;
   CFL_UINT32 ulHash;
   char       *data;
};

extern CFL_STRP cfl_str_new(CFL_UINT32  iniCapacity);
extern CFL_STRP cfl_str_newBuffer(const char *buffer);
extern CFL_STRP cfl_str_newBufferLen(const char *buffer, CFL_UINT32 len);
extern CFL_STRP cfl_str_newStr(CFL_STRP str);
extern void cfl_str_free(CFL_STRP sb);
extern CFL_STRP cfl_str_append(CFL_STRP sb, const char * str, ...);
extern CFL_STRP cfl_str_appendChar(CFL_STRP sb, char c);
extern CFL_STRP cfl_str_appendLen(CFL_STRP sb, const char *str, int len);
extern CFL_STRP cfl_str_appendStr(CFL_STRP sb, CFL_STRP str);
extern char *cfl_str_getPtr(CFL_STRP sb);
extern CFL_UINT32 cfl_str_getLength(CFL_STRP sb);
extern void cfl_str_clear(CFL_STRP sb);
extern void cfl_str_setValue(CFL_STRP sb, const char *str);
extern void cfl_str_setValueLen(CFL_STRP sb, const char *str, int len);
      
extern CFL_BOOL cfl_str_bufferStartsWith(CFL_STRP sb, const char *str);
extern CFL_BOOL cfl_str_bufferStartsWithIgnoreCase(CFL_STRP sb, const char *str);
extern CFL_BOOL cfl_str_equals(CFL_STRP str1, CFL_STRP str2);
extern CFL_BOOL cfl_str_equalsIgnoreCase(CFL_STRP str1, CFL_STRP str2);
extern CFL_INT16 cfl_str_compare(CFL_STRP str1, CFL_STRP str2, CFL_BOOL bExact);
extern CFL_INT16 cfl_str_compareIgnoreCase(CFL_STRP str1, CFL_STRP str2, CFL_BOOL bExact);
extern CFL_BOOL cfl_str_bufferEquals(CFL_STRP str1, const char *str2);
extern CFL_BOOL cfl_str_bufferEqualsIgnoreCase(CFL_STRP str1, const char *str2);
extern CFL_INT16 cfl_str_bufferCompare(CFL_STRP str1, const char *str2, CFL_BOOL bExact);
extern CFL_INT16 cfl_str_bufferCompareIgnoreCase(CFL_STRP str1, const char *str2, CFL_BOOL bExact);

extern CFL_UINT32 cfl_str_hashCode(CFL_STRP str);
extern CFL_STRP cfl_str_toUpper(CFL_STRP str);
extern CFL_STRP cfl_str_toLower(CFL_STRP str);
extern CFL_STRP cfl_str_trim(CFL_STRP str);

extern CFL_BOOL cfl_str_isEmpty(CFL_STRP str);
extern CFL_STRP cfl_str_substr(CFL_STRP sb, CFL_UINT32 start, CFL_UINT32 end);
extern CFL_INT32 cfl_str_bufferFind(CFL_STRP sb, const char *str, CFL_UINT32 len);

#endif
