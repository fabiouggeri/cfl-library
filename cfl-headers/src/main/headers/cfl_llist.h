#ifndef CFL_LLIST_H_

#define CFL_LLIST_H_

#include "cfl_types.h"

typedef struct _CFL_LNODE {
   void *data;
   struct _CFL_LNODE *previous;
   struct _CFL_LNODE *next;
} CFL_LNODE, *CFL_LNODEP;

struct _CFL_LLIST {
   CFL_UINT32 maxNodeCache;
   CFL_UINT32 nodeCount;
   CFL_LNODEP first;
   CFL_LNODEP last;
   CFL_LNODEP head;
   CFL_LNODEP tail;
};

extern CFL_LLISTP cfl_llist_new(CFL_UINT32 maxNodeCache);
extern void cfl_llist_free(CFL_LLISTP list);
extern void cfl_llist_addLast(CFL_LLISTP list, void *item);
extern void cfl_llist_addFirst(CFL_LLISTP list, void *item);
extern void *cfl_llist_getLast(CFL_LLISTP list);
extern void *cfl_llist_getFirst(CFL_LLISTP list);
extern void *cfl_llist_removeLast(CFL_LLISTP list);
extern void *cfl_llist_removeFirst(CFL_LLISTP list);

#endif

