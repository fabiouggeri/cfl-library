#ifndef _CFL_ITERATOR_H_

#define _CFL_ITERATOR_H_

#if defined( __cplusplus ) && ! defined( __IBMCPP__ )
   extern "C" {
#endif

#include "cfl_types.h"

struct _CFL_ITERATOR_CLASS {
   CFL_BOOL (*has_next)(CFL_ITERATORP it);
   void *(*next)(CFL_ITERATORP it);
   void *(*current_value)(CFL_ITERATORP it);
   void (*remove)(CFL_ITERATORP it);
   void (*free)(CFL_ITERATORP it);
   void (*first)(CFL_ITERATORP it);
   CFL_BOOL (*has_previous)(CFL_ITERATORP it);
   void *(*previous)(CFL_ITERATORP it);
   void (*last)(CFL_ITERATORP it);
   void (*add)(CFL_ITERATORP it, void *value);
};

struct _CFL_ITERATOR {
   CFL_ITERATOR_CLASS *itClass;
};

extern CFL_ITERATORP cfl_iterator_new(void);
extern void cfl_iterator_free(CFL_ITERATORP it);
extern CFL_BOOL cfl_iterator_hasPrevious(CFL_ITERATORP it);
extern CFL_BOOL cfl_iterator_hasNext(CFL_ITERATORP it);
extern void *cfl_iterator_next(CFL_ITERATORP it);
extern void *cfl_iterator_value(CFL_ITERATORP it);
extern void cfl_iterator_remove(CFL_ITERATORP it);
extern void cfl_iterator_first(CFL_ITERATORP it); 
extern void *cfl_iterator_previous(CFL_ITERATORP it);
extern void cfl_iterator_last(CFL_ITERATORP it); 
extern void cfl_iterator_add(CFL_ITERATORP it, void *value);

#if defined( __cplusplus ) && ! defined( __IBMCPP__ )
   }
#endif

#endif