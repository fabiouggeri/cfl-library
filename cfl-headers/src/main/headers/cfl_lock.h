#ifndef CFL_LOCK_H_

#define CFL_LOCK_H_

#include "cfl_types.h"

#if defined(__BORLANDC__)
   #define _CFL_LIGHT_LOCK
#endif

struct _CFL_LOCK {
#ifdef _CFL_LIGHT_LOCK
   CFL_INT32  handle; 
   CFL_UINT32 lockChange;
#else
   #ifdef SRWLOCK_INIT
      CFL_SLIM_LOCK_HANDLE handle;
   #else
      CFL_CRITICAL_SECTION handle;
   #endif
      CFL_CONDITION_VARIABLE lockChange;
#endif      
   CFL_UINT32         lockCount;
   CFL_THREAD_ID      threadId;
   CFL_UINT32         timeout;
   CFL_BOOL           isAllocated;
#ifdef _CFL_LIGHT_LOCK
   char padding[3];
#endif
};

extern CFL_LOCKP cfl_lock_new(void);
extern void cfl_lock_free(CFL_LOCKP pLock);
extern void cfl_lock_init(CFL_LOCKP pLock);
extern void cfl_lock_acquireExclusive(CFL_LOCKP pLock);
extern void cfl_lock_acquireShared(CFL_LOCKP pLock);
extern void cfl_lock_release(CFL_LOCKP pLock);

#endif