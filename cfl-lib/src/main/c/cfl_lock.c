#include <stdlib.h>

#include "cfl_lock.h"
#include "cfl_hash.h"

#define EXCLUSIVE_LOCK 0xFFFFFFFF

#define IS_LOCKED_BY_ME(l) (l)->threadId == GetCurrentThreadId()

#ifdef _CFL_LIGHT_LOCK
   #define INITIALIZE_LOCK_HANDLE(l)   (l)->handle=0
   #define RELEASE_LOCK_HANDLE(l)      // do nothing 
   #define ACQUIRE_EXCLUSIVE(l)        interlockedAcquire(l)
   #define RELEASE_EXCLUSIVE(l)        interlockedRelease(l)
   #define SLEEP_CONDITION(l)          interlockedRelease(l);interlockedAcquire(l)
   #define INITIALIZE_CONDITION_VAR(l) // do nothing 
   #define RELEASE_CONDITION_VAR(l)    // do nothing 
   #define WAKE_ALL_CONDITION(l)       // do nothing 
#else
   #ifdef SRWLOCK_INIT
      #define INITIALIZE_LOCK_HANDLE(l) InitializeSRWLock(&(l)->handle)
      #define RELEASE_LOCK_HANDLE(l)    // do nothing 
      #define ACQUIRE_EXCLUSIVE(l)      AcquireSRWLockExclusive(&(l)->handle)
      #define RELEASE_EXCLUSIVE(l)      ReleaseSRWLockExclusive(&(l)->handle)
      #define SLEEP_CONDITION(l)        SleepConditionVariableSRW(&(l)->lockChange, &(l)->handle, (l)->timeout, 0)
   #else
      #define INITIALIZE_LOCK_HANDLE(l) InitializeCriticalSection(&(l)->handle)
      #define RELEASE_LOCK_HANDLE(l)    DeleteCriticalSection(&(l)->handle)
      #define ACQUIRE_EXCLUSIVE(l)      EnterCriticalSection(&(l)->handle)
      #define RELEASE_EXCLUSIVE(l)      LeaveCriticalSection(&(l)->handle)
      #define SLEEP_CONDITION(l)        SleepConditionVariableCS(&(l)->lockChange, &(l)->handle, (l)->timeout, 0)
   #endif
   #define INITIALIZE_CONDITION_VAR(l)  InitializeConditionVariable(&(l)->lockChange)
   #define RELEASE_CONDITION_VAR(l)     // do nothing 
   #define WAKE_ALL_CONDITION(l)        WakeAllConditionVariable(&(l)->lockChange)
#endif

#ifdef _CFL_LIGHT_LOCK
static void interlockedAcquire(CFL_LOCKP lock) {
   int retry = 0;
   do {
      LONG prevHandle = lock->handle;
      if (prevHandle == 0) {
         if (InterlockedCompareExchange(&lock->handle, 1, 0) == 0) {
            return;
         }
      }

      if (++retry > 30) {
         retry = 0;
         Sleep(lock->timeout);
      }
   } while (CFL_TRUE);
}

static void interlockedRelease(CFL_LOCKP lock) {
   lock->handle = 0;
}
#endif

CFL_LOCKP cfl_lock_new(void) {
   CFL_LOCKP lock = malloc(sizeof(CFL_LOCK));
   cfl_lock_init(lock);
   lock->isAllocated = CFL_TRUE;
   return lock;
}

void cfl_lock_free(CFL_LOCKP pLock) {
   if (pLock && pLock->isAllocated) {
      RELEASE_LOCK_HANDLE(pLock);
      RELEASE_CONDITION_VAR(pLock);
      free(pLock);
   }
}

void cfl_lock_init(CFL_LOCKP pLock) {
   INITIALIZE_LOCK_HANDLE(pLock);
   INITIALIZE_CONDITION_VAR(pLock);
   pLock->lockCount = 0;
   pLock->threadId = 0;
   pLock->timeout = 100;
   pLock->isAllocated = CFL_FALSE;
}

void cfl_lock_acquireExclusive(CFL_LOCKP pLock) {
   if (pLock) {
      CFL_BOOL bLocked;
      ACQUIRE_EXCLUSIVE(pLock);
      do {
         if (pLock->lockCount == 0) {
            bLocked = CFL_TRUE;
            pLock->lockCount = EXCLUSIVE_LOCK;
            pLock->threadId = GetCurrentThreadId();
         } else if (pLock->lockCount == EXCLUSIVE_LOCK && IS_LOCKED_BY_ME(pLock)) {
            bLocked = CFL_TRUE;
         } else {
            bLocked = CFL_FALSE;
            SLEEP_CONDITION(pLock);
         }
      } while (! bLocked);
      RELEASE_EXCLUSIVE(pLock);
      WAKE_ALL_CONDITION(pLock);
   }
}

void cfl_lock_acquireShared(CFL_LOCKP pLock) {
   if (pLock) {
      CFL_BOOL bLocked;
      ACQUIRE_EXCLUSIVE(pLock);
      do {
         if (pLock->lockCount != EXCLUSIVE_LOCK) {
            bLocked = CFL_TRUE;
            ++pLock->lockCount;
         } else if (IS_LOCKED_BY_ME(pLock)) {
            bLocked = CFL_TRUE;
         } else {
            bLocked = CFL_FALSE;
            SLEEP_CONDITION(pLock);
         }
      } while (! bLocked);
      RELEASE_EXCLUSIVE(pLock);
      WAKE_ALL_CONDITION(pLock);
   }
}

void cfl_lock_release(CFL_LOCKP pLock) {
   if (pLock) {
      ACQUIRE_EXCLUSIVE(pLock);
      if (pLock->lockCount == EXCLUSIVE_LOCK) {
         if (IS_LOCKED_BY_ME(pLock)) {
            pLock->lockCount = 0;
            pLock->threadId = 0;
         }
      } else if (pLock->lockCount > 0) {
         --pLock->lockCount;
      }
      RELEASE_EXCLUSIVE(pLock);
      WAKE_ALL_CONDITION(pLock);
   }
}
