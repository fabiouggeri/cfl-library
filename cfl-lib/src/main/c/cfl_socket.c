#include "cfl_socket.h"
#include "cfl_types.h"
#include "cfl_buffer.h"

CFL_SOCKET cfl_socket_open(const char *serverAddress, CFL_UINT16 port) {
   /* If not Borland C. */
   #if ! defined(__BORLANDC__)
      int retVal;
      CFL_SOCKET socketHandle = CFL_INVALID_SOCKET;
      struct addrinfo *addr;
      struct addrinfo addrCriteria;
      struct addrinfo *serverAddr;
      char serverPort[7];
      WSADATA winsockinfo;

      retVal = WSAStartup(MAKEWORD(2, 2), &winsockinfo);
      if (retVal != 0) {
         return CFL_INVALID_SOCKET;
      }

      memset(&addrCriteria, 0, sizeof (addrCriteria));
      addrCriteria.ai_family = AF_UNSPEC;
      addrCriteria.ai_socktype = SOCK_STREAM;
      addrCriteria.ai_protocol = IPPROTO_TCP;

      // Get address(es)
      sprintf(serverPort, "%u", port);
      retVal = getaddrinfo(serverAddress, serverPort, &addrCriteria, &serverAddr);
      if (retVal != 0) {
         return CFL_INVALID_SOCKET;
      }

      for (addr = serverAddr; addr != NULL; addr = addr->ai_next) {
         // Create a reliable, stream socket using TCP
         socketHandle = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
         if (socketHandle == CFL_INVALID_SOCKET) {
            continue;
         }

         // Establish the connection to the echo server
         if (connect(socketHandle, addr->ai_addr, (int) addr->ai_addrlen) == 0) {
            break;
         }

         closesocket(socketHandle);
         socketHandle = CFL_INVALID_SOCKET;
      }

      freeaddrinfo(serverAddr); // Free addrinfo allocated in getaddrinfo()
      return socketHandle;
   #else   
      int retVal;
      CFL_SOCKET socketHandle;
      struct sockaddr_in addr;
      WSADATA winsockinfo;

      retVal = WSAStartup(MAKEWORD(2, 2), &winsockinfo);
      if (retVal != 0) {
         return CFL_INVALID_SOCKET;
      }

      socketHandle = (CFL_SOCKET) socket(AF_INET, SOCK_STREAM, 0);
      if (socketHandle == CFL_INVALID_SOCKET) {
         return CFL_INVALID_SOCKET;
      }

      memset(&addr, 0, sizeof(addr));
      addr.sin_family      = AF_INET;
      addr.sin_addr.s_addr = inet_addr(serverAddress);
      addr.sin_port        = htons(port);

      retVal = connect(socketHandle, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
      if (retVal < 0) {
         closesocket(socketHandle);
         return CFL_INVALID_SOCKET;
      }
      return socketHandle;
   #endif
}

CFL_INT32 cfl_socket_close(CFL_SOCKET socket) {
   CFL_INT32 rc;
   rc = closesocket(socket);
   WSACleanup();
   return rc;
}

CFL_INT32 cfl_socket_send(CFL_SOCKET socket, char *buffer, int len) {
   return send(socket, buffer, len, 0);
}

CFL_BOOL cfl_socket_sendAll(CFL_SOCKET socket, CFL_BUFFERP buffer) {
   CFL_UINT32 totalSent = 0; // how many bytes we've sent
   CFL_UINT32 bytesLeft;
   CFL_INT32 bytesSent;
   const char *data;

   bytesLeft = cfl_buffer_remaining(buffer);
   data = (const char *) cfl_buffer_getDataPtr(buffer);
   while (bytesLeft > 0) {
      bytesSent = send(socket, data + totalSent, (int) bytesLeft, 0);
      if (bytesSent < 0) {
         break;
      }
      totalSent += bytesSent;
      bytesLeft -= bytesSent;
   }
   cfl_buffer_skip(buffer, totalSent);
   return bytesLeft == 0;
}

CFL_INT32 cfl_socket_receive(CFL_SOCKET socket, char *buffer, int len) {
   int retVal;
   int toRead = len;
   
   while (toRead > 0) {
      retVal = recv(socket, buffer, toRead, 0);
      if (retVal <= 0) {
         return retVal;
      }
      buffer += retVal;
      toRead -= retVal;
   }
   return len;
}

CFL_BOOL cfl_socket_receiveAll(CFL_SOCKET socket, CFL_BUFFERP buffer, CFL_UINT32 packetLen, CFL_UINT32 timeout) {
   int retVal;
   CFL_UINT32 bodyLen;
   char *dataRead;
   int readLen;

   // wait until timeout or data received
   retVal = cfl_socket_selectRead(socket, timeout, 0);
   if (retVal <= 0) {
      return CFL_FALSE;
   }
  
   dataRead = (char *) malloc(packetLen * sizeof(char));
   if (dataRead == NULL) {
      return CFL_FALSE;
   }
   // data must be here, so do a normal recv()
   bodyLen = cfl_buffer_remaining(buffer);
   while (bodyLen > 0) {
      readLen = bodyLen > packetLen ? packetLen : bodyLen;
      retVal = recv(socket, dataRead, readLen, 0);
      if (retVal == CFL_SOCKET_ERROR) {
         free(dataRead);
         return CFL_FALSE;
      } else if (retVal == 0) {
         free(dataRead);
         return CFL_FALSE;
      }
      bodyLen -= retVal;
      cfl_buffer_put(buffer, dataRead, retVal);
   }
   free(dataRead);
   return CFL_TRUE;   
}

CFL_INT32 cfl_socket_selectRead(CFL_SOCKET socket, long sec, long mSec) {
   fd_set fdSet;
   struct timeval time;

   FD_ZERO(&fdSet);
   FD_SET(socket, &fdSet);

   time.tv_sec = sec;
   time.tv_usec = mSec;
   return select((int) socket + 1, &fdSet, NULL, NULL, &time);
}

CFL_INT32 cfl_socket_selectWrite(CFL_SOCKET socket, long sec, long mSec) {
   fd_set fdSet;
   struct timeval time;

   FD_ZERO(&fdSet);
   FD_SET(socket, &fdSet);

   time.tv_sec = sec;
   time.tv_usec = mSec;
   return select((int) socket + 1, NULL, &fdSet, NULL, &time);
}

CFL_INT32 cfl_socket_lastErrorCode() {
   return WSAGetLastError();
}

CFL_BOOL cfl_socket_setBlockingMode(CFL_SOCKET socket, CFL_BOOL block) {
   u_long mode = block ? 1 : 0;
   return ioctlsocket(socket, FIONBIO, &mode) != NO_ERROR;
}
