#include <stdlib.h>
#include "cfl_iterator.h"

CFL_ITERATORP cfl_iterator_new(void) {
   return (CFL_ITERATORP) malloc(sizeof(CFL_ITERATOR));
}

void cfl_iterator_free(CFL_ITERATORP it) {
   if (it->itClass->free == NULL) {
      free(it);
   } else {
      it->itClass->free(it);
   }
}

CFL_BOOL cfl_iterator_hasNext(CFL_ITERATORP it) {
   return it->itClass->has_next(it);
}

CFL_BOOL cfl_iterator_hasPrevious(CFL_ITERATORP it) {
   return it->itClass->has_previous != NULL && it->itClass->has_previous(it);
}

void * cfl_iterator_next(CFL_ITERATORP it) {
   return it->itClass->next(it);
}

void * cfl_iterator_value(CFL_ITERATORP it) {
   if (it->itClass->current_value != NULL) {
      return it->itClass->current_value(it);
   }
   return NULL;
}

void * cfl_iterator_previous(CFL_ITERATORP it) {
   if (it->itClass->previous != NULL) {
      return it->itClass->previous(it);
   }
   return NULL;
}

void cfl_iterator_remove(CFL_ITERATORP it) {
   if (it->itClass->remove != NULL) {
      it->itClass->remove(it);
   }
}

void cfl_iterator_add(CFL_ITERATORP it, void *value) {
   if (it->itClass->add != NULL) {
      it->itClass->add(it, value);
   }
}

void cfl_iterator_first(CFL_ITERATORP it) {
   if (it->itClass->first != NULL) {
      it->itClass->first(it);
   }
}

void cfl_iterator_last(CFL_ITERATORP it) {
   if (it->itClass->last != NULL) {
      it->itClass->last(it);
   }
}

