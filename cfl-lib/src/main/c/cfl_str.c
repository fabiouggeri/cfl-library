#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "cfl_str.h"

static void ensureCapacity(CFL_STRP sb, CFL_UINT32 newLen) {
   if (newLen > sb->ulCapacity) {
      sb->ulCapacity = (sb->ulCapacity >> 1) + 1 + newLen;
      sb->data = (char *) realloc(sb->data, sb->ulCapacity);
   }
}

CFL_STRP cfl_str_new(CFL_UINT32 iniCapacity) {
   CFL_STRP sb;
   sb = (CFL_STRP) malloc(sizeof (CFL_STR));
   sb->objectType = CFL_TYPE_STRING;
   sb->ulCapacity = iniCapacity;
   sb->ulLength = 0;
   sb->ulHash = 0;
   sb->data = (char *) malloc(iniCapacity * sizeof (char));
   sb->data[0] = '\0';
   return sb;
}

CFL_STRP cfl_str_newBuffer(const char *buffer) {
   CFL_UINT32 len;
   CFL_STRP sb;
   sb = (CFL_STRP) malloc(sizeof (CFL_STR));
   len = (CFL_UINT32) strlen(buffer);
   sb->objectType = CFL_TYPE_STRING;
   sb->ulCapacity = len + 1;
   sb->ulLength = len;
   sb->ulHash = 0;
   sb->data = (char *) malloc(sizeof (char) * (len + 1));
   memcpy(sb->data, (void *) buffer, len * sizeof (char));
   sb->data[len] = '\0';
   return sb;
}

CFL_STRP cfl_str_newBufferLen(const char *buffer, CFL_UINT32 len) {
   CFL_STRP sb;
   sb = (CFL_STRP) malloc(sizeof (CFL_STR));
   sb->objectType = CFL_TYPE_STRING;
   sb->ulCapacity = (CFL_UINT32) len + 1;
   sb->ulLength = (CFL_UINT32) len;
   sb->ulHash = 0;
   sb->data = (char *) malloc(sizeof (char) * (len + 1));
   memcpy(sb->data, (void *) buffer, len * sizeof (char));
   sb->data[len] = '\0';
   return sb;
}

CFL_STRP cfl_str_newStr(CFL_STRP str) {
   CFL_STRP sb;
   sb = (CFL_STRP) malloc(sizeof (CFL_STR));
   sb->objectType = CFL_TYPE_STRING;
   sb->ulCapacity = str->ulLength + 1;
   sb->ulLength = str->ulLength;
   sb->ulHash = 0;
   sb->data = (char *) malloc(sizeof (char) * (str->ulCapacity));
   memcpy(sb->data, (void *) str->data, str->ulLength * sizeof (char));
   sb->data[str->ulLength] = '\0';
   return sb;
}

void cfl_str_free(CFL_STRP sb) {
   free(sb->data);
   free(sb);
}

CFL_STRP cfl_str_appendChar(CFL_STRP sb, char c) {
   ensureCapacity(sb, sb->ulLength + 2);
   sb->data[sb->ulLength++] = c;
   sb->data[sb->ulLength] = '\0';
   sb->ulHash = 0;
   return sb;
}

CFL_STRP cfl_str_append(CFL_STRP sb, const char * str, ...) {
   CFL_UINT32 len;
   va_list va;
   char * strPtr = (char *) str;

   va_start(va, str);
   while (strPtr) {
      len = (CFL_UINT32) strlen(strPtr);
      if (len > 0) {
         ensureCapacity(sb, sb->ulLength + len + 1);
         memcpy(&sb->data[sb->ulLength], (void *) strPtr, len * sizeof (char));
         sb->ulLength += len;
         sb->data[sb->ulLength] = '\0';
      }
      strPtr = va_arg(va, char *);
   }
   va_end(va);
   sb->ulHash = 0;
   return sb;
}

CFL_STRP cfl_str_appendLen(CFL_STRP sb, const char *str, int len) {
   if (len > 0) {
      ensureCapacity(sb, sb->ulLength + len + 1);
      memcpy(&sb->data[sb->ulLength], (void *) str, len * sizeof (char));
      sb->ulLength += len;
      sb->data[sb->ulLength] = '\0';
      sb->ulHash = 0;
   }
   return sb;
}

CFL_STRP cfl_str_appendStr(CFL_STRP sb, CFL_STRP str) {
   if (str->ulLength > 0) {
      ensureCapacity(sb, sb->ulLength + str->ulLength + 1);
      memcpy(&sb->data[sb->ulLength], (void *) str->data, str->ulLength * sizeof (char));
      sb->ulLength += str->ulLength;
      sb->data[sb->ulLength] = '\0';
      sb->ulHash = 0;
   }
   return sb;
}

char *cfl_str_getPtr(CFL_STRP sb) {
   return sb->data;
}

CFL_UINT32 cfl_str_getLength(CFL_STRP sb) {
   return sb->ulLength;
}

void cfl_str_clear(CFL_STRP sb) {
   sb->ulLength = 0;
   sb->ulHash = 0;
   sb->data[0] = '\0';
}

void cfl_str_setValue(CFL_STRP sb, const char *str) {
   CFL_UINT32 len = (CFL_UINT32) strlen(str);
   if (len > 0) {
      ensureCapacity(sb, len + 1);
      memcpy(sb->data, (void *) str, len * sizeof (char));
   }
   sb->ulLength = len;
   sb->ulHash = 0;
   sb->data[len] = '\0';
}

void cfl_str_setValueLen(CFL_STRP sb, const char *str, int len) {
   if (len > 0) {
      ensureCapacity(sb, len + 1);
      memcpy(sb->data, (void *) str, len * sizeof (char));
   }
   sb->ulLength = len;
   sb->ulHash = 0;
   sb->data[len] = '\0';
}

CFL_INT16 cfl_str_compare(CFL_STRP str1, CFL_STRP str2, CFL_BOOL bExact) {
   char *s1;
   char *s2;
   int c1;
   int c2;

   if (str1 == str2) {
      return 0;
   }
   
   s1 = str1->data;
   s2 = str2->data;
   do {
      c1 = (int) *s1;
      c2 = (int) *s2;

      if (c1 < c2) {
         return c1 == '\0' && !bExact ? 0 : -1;
      } else if (c1 > c2) {
         return c2 == '\0' && !bExact ? 0 : 1;
      }

      s1++;
      s2++;
   } while (c1);
   return 0;
}

CFL_INT16 cfl_str_compareIgnoreCase(CFL_STRP str1, CFL_STRP str2, CFL_BOOL bExact) {
   char *s1;
   char *s2;
   int c1;
   int c2;

   s1 = str1->data;
   s2 = str2->data;
   if (str1 == str2) {
      return 0;
   }
   
   do {
      c1 = toupper((int) *s1);
      c2 = toupper((int) *s2);

      if (c1 < c2) {
         return c1 == '\0' && !bExact ? 0 : -1;
      } else if (c1 > c2) {
         return c2 == '\0' && !bExact ? 0 : 1;
      }

      s1++;
      s2++;
   } while (c1);
   return 0;
}

CFL_BOOL cfl_str_bufferStartsWith(CFL_STRP sb, const char *str) {
   char *s1 = sb->data;
   char *s2 = (char *) str;
   int c1;
   int c2;

   do {
      c1 = (int) *s1;
      c2 = (int) *s2;

      if (c2 == '\0') {
         return CFL_TRUE;
      } else if (c2 != c1) {
         return CFL_FALSE;
      }
      s1++;
      s2++;
   } while (c1);
   return CFL_FALSE;
}

CFL_BOOL cfl_str_bufferStartsWithIgnoreCase(CFL_STRP sb, const char *str) {
   char *s1 = sb->data;
   char *s2 = (char *) str;
   int c1;
   int c2;

   do {
      c1 = toupper((int) *s1);
      c2 = toupper((int) *s2);

      if (c2 == '\0') {
         return CFL_TRUE;
      } else if (c2 != c1) {
         return CFL_FALSE;
      }
      s1++;
      s2++;
   } while (c1);
   return CFL_FALSE;
}

CFL_BOOL cfl_str_equals(CFL_STRP str1, CFL_STRP str2) {
   if (str1->ulLength != str2->ulLength) {
      return CFL_FALSE;
   }
   return cfl_str_compare(str1, str2, CFL_TRUE) == 0;
}

CFL_BOOL cfl_str_equalsIgnoreCase(CFL_STRP str1, CFL_STRP str2) {
   if (str1->ulLength != str2->ulLength) {
      return CFL_FALSE;
   }
   return cfl_str_compareIgnoreCase(str1, str2, CFL_TRUE) == 0;
}

CFL_INT16 cfl_str_bufferCompare(CFL_STRP str1, const char *s2, CFL_BOOL bExact) {
   char *s1 = str1->data;
   int c1;
   int c2;

   do {
      c1 = (int) *s1;
      c2 = (int) *s2;

      if (c1 < c2) {
         return c1 == '\0' && !bExact ? 0 : -1;
      } else if (c1 > c2) {
         return c2 == '\0' && !bExact ? 0 : 1;
      }

      s1++;
      s2++;
   } while (c1);
   return 0;
}

CFL_INT16 cfl_str_bufferCompareIgnoreCase(CFL_STRP str1, const char *s2, CFL_BOOL bExact) {
   char *s1 = str1->data;
   int c1;
   int c2;

   do {
      c1 = toupper((int) *s1);
      c2 = toupper((int) *s2);

      if (c1 < c2) {
         return c1 == '\0' && !bExact ? 0 : -1;
      } else if (c1 > c2) {
         return c2 == '\0' && !bExact ? 0 : 1;
      }

      s1++;
      s2++;
   } while (c1);
   return 0;
}

CFL_BOOL cfl_str_bufferEquals(CFL_STRP str1, const char *str2) {
   return cfl_str_bufferCompare(str1, str2, CFL_TRUE) == 0;
}

CFL_BOOL cfl_str_bufferEqualsIgnoreCase(CFL_STRP str1, const char *str2) {
   return cfl_str_bufferCompareIgnoreCase(str1, str2, CFL_TRUE) == 0;
}

CFL_UINT32 cfl_str_hashCode(CFL_STRP str) {
   if (str->ulHash == 0 && str->ulLength > 0) {
      CFL_UINT32 hash = 0;
      CFL_UINT32 i;
      for (i = 0; i < str->ulLength; i++) {
         hash = 31 * hash + str->data[i];
      }
      str->ulHash = hash;
   }
   return str->ulHash;
}

// murmur3 hash algorithm
//CFL_UINT32 cfl_str_hashCode(CFL_STRP str) {
//   if (str->ulHash == 0 && str->ulLength > 0) {
//      CFL_UINT8* key = str->data;
//      CFL_UINT32 hash = (CFL_UINT32) key[0];
//      size_t len = (size_t) str->ulLength;
//      if (len > 3) {
//         const CFL_UINT32* key_x4 = (const CFL_UINT32*) key;
//         size_t i = len >> 2;
//         do {
//            CFL_UINT32 k = *key_x4++;
//            k *= 0xcc9e2d51;
//            k = (k << 15) | (k >> 17);
//            k *= 0x1b873593;
//            hash ^= k;
//            hash = (hash << 13) | (hash >> 19);
//            hash = (hash * 5) + 0xe6546b64;
//         } while (--i);
//         key = (const CFL_UINT8*) key_x4;
//      }
//      if (len & 3) {
//         size_t i = len & 3;
//         CFL_UINT32 k = 0;
//         key = &key[i - 1];
//         do {
//            k <<= 8;
//            k |= *key--;
//         } while (--i);
//         k *= 0xcc9e2d51;
//         k = (k << 15) | (k >> 17);
//         k *= 0x1b873593;
//         hash ^= k;
//      }
//      hash ^= len;
//      hash ^= hash >> 16;
//      hash *= 0x85ebca6b;
//      hash ^= hash >> 13;
//      hash *= 0xc2b2ae35;
//      hash ^= hash >> 16;
//      str->ulHash = hash;
//   }
//   return str->ulHash;
//}

CFL_STRP cfl_str_toUpper(CFL_STRP str) {
   CFL_UINT32 i;
   for (i = 0; i < str->ulLength; i++) {
      if (CFL_ISLOWER(str->data[i])) {
         str->data[i] = CFL_TOUPPER(str->data[i]);
      }
   }
   return str;
}

CFL_STRP cfl_str_toLower(CFL_STRP str) {
   CFL_UINT32 i;
   for (i = 0; i < str->ulLength; i++) {
      if (CFL_ISUPPER(str->data[i])) {
         str->data[i] = CFL_TOLOWER(str->data[i]);
      }
   }
   return str;
}

CFL_STRP cfl_str_trim(CFL_STRP str) {
   if (str->ulLength > 0) {
      CFL_UINT32 start = 0;
      CFL_UINT32 end;
      while (str->data[start] && str->data[start] == ' ') {
         ++start;
      }
      end = str->ulLength - 1;
      while (str->data[end] == ' ' && end > start) {
         --end;
      }
      ++end;
      if (start > end) {
         memmove(str->data, &str->data[start], end - start);
      }
      end -= start;
      if (end < str->ulLength) {
         str->data[end] = '\0';
         str->ulLength = end;
      }
   }
   return str;
}

CFL_BOOL cfl_str_isEmpty(CFL_STRP str) {
   if (str && str->ulLength > 0) {
      return CFL_FALSE;
   }
   return CFL_TRUE;
}

CFL_STRP cfl_str_substr(CFL_STRP sb, CFL_UINT32 start, CFL_UINT32 end) {
   CFL_STRP subs = NULL;
   if (start < sb->ulLength) {
      if (end > sb->ulLength) {
         subs = cfl_str_newBufferLen(&sb->data[start], sb->ulLength - start);
      } else {
         subs = cfl_str_newBufferLen(&sb->data[start], end - start);
      }
   }
   return subs;
}
