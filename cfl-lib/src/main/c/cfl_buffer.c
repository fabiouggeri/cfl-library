#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cfl_buffer.h"
#include "cfl_str.h"

#define BUFFER_INI_SIZE 8192

#define ENDIANNESS    ((*(char*)&i) == 0 ? BIG_ENDIAN : LITTLE_ENDIAN)

#define PUT_BUFFER(t, b, v) \
         ensureCapacity(b, b->length + sizeof(t));\
         *((t *)&b->data[b->position]) = v;\
         b->position += sizeof(t);\
         if (b->position > b->length) b->length = b->position

#define GET_BUFFER(t, b) \
         *((t *)&b->data[b->position]); \
         b->position += sizeof(t)
         
//#define GET_NUM_BUFFER(t, b) \
//         *((t *)swapBytes(b, &b->data[b->position], sizeof(t)));\
//         b->position += sizeof(t)
         
#define GET_NUM_BUFFER(t, b) \
         *((t *)&b->data[b->position]);\
         b->position += sizeof(t)
         
#define RETURN_BUFFER(t, b) \
         b->position += sizeof(t); \
         if (b->position > b->length) b->length = b->position; \
         return *((t *)&b->data[b->position - sizeof(t)])
         
//#define RETURN_NUM_BUFFER(t, b) \
//         b->position += sizeof(t); \
//         if (b->position > b->length) b->length = b->position; \
//         return *((t *)swapBytes(b, &b->data[b->position - sizeof(t)], sizeof(t)))
       
#define RETURN_NUM_BUFFER(t, b) \
         b->position += sizeof(t); \
         if (b->position > b->length) b->length = b->position; \
         return *((t *)&b->data[b->position - sizeof(t)])

const int i = 1;

static void *swapBytes(CFL_BUFFERP buffer, void *data, CFL_UINT32 size) {
   if (ENDIANNESS != buffer->endian) {
      char aux;
      char *bytes = (char *) data;
      CFL_UINT32 i;
      for (i = 0; i < size / 2; ++i) {
         aux = bytes[size - 1 - i];
         bytes[size - 1 - i] = bytes[i];
         bytes[i] = aux;
      }      
   }
   return data;
}

static void ensureCapacity(CFL_BUFFERP buffer, CFL_UINT32 newCapacity) {
   if (newCapacity > buffer->capacity) {
      buffer->data = (CFL_UINT8 *) realloc(buffer->data, newCapacity * sizeof(CFL_UINT8));
      buffer->capacity = newCapacity;
   }
}

CFL_BUFFERP cfl_buffer_new(void) {
   CFL_BUFFERP buffer;
   buffer = (CFL_BUFFERP) malloc(sizeof(CFL_BUFFER));
   buffer->capacity = BUFFER_INI_SIZE;
   buffer->length = 0;
   buffer->position = 0;
   buffer->endian = ENDIANNESS;
   buffer->data = (CFL_UINT8 *) malloc(BUFFER_INI_SIZE * sizeof(CFL_UINT8));
   return buffer;
}

void cfl_buffer_free(CFL_BUFFERP buffer) {
   if (buffer) {
      if (buffer->data) {
         free(buffer->data);
      }
      free(buffer);
   }
}

CFL_UINT8 *cfl_buffer_getDataPtr(CFL_BUFFERP buffer) {
   return buffer->data;
}

void cfl_buffer_setBigEndian(CFL_BUFFERP buffer, CFL_BOOL bigEndian) {
   if (buffer != NULL) {
      buffer->endian = bigEndian ? BIG_ENDIAN : LITTLE_ENDIAN;
   }
}

CFL_BOOL cfl_buffer_isBigEndian(CFL_BUFFERP buffer) {
   if (buffer != NULL) {
      return buffer->endian == BIG_ENDIAN;
   }
   return CFL_FALSE;
}

void cfl_buffer_flip(CFL_BUFFERP buffer) {
   buffer->length = buffer->position;
   buffer->position = 0;
}

CFL_UINT32 cfl_buffer_length(CFL_BUFFERP buffer) {
   return buffer->length;
}

void cfl_buffer_setLength(CFL_BUFFERP buffer, CFL_UINT32 newLen) {
   if (newLen > buffer->length) {
      if (newLen > buffer->capacity) {
         ensureCapacity(buffer, newLen);
      }
      buffer->length = newLen;
   } else {
      buffer->length = newLen;
      if (buffer->position > newLen) {
         buffer->position = newLen;
      }
   }
}

void cfl_buffer_reset(CFL_BUFFERP buffer) {
   buffer->length = 0;
   buffer->position = 0;
}

CFL_UINT32 cfl_buffer_position(CFL_BUFFERP buffer) {
   return buffer->position;
}

void cfl_buffer_setPosition(CFL_BUFFERP buffer, CFL_UINT32 newPos) {
   buffer->position = newPos;
}

void cfl_buffer_skip(CFL_BUFFERP buffer, CFL_UINT32 skip) {
   buffer->position += skip;
}

void cfl_buffer_rewind(CFL_BUFFERP buffer) {
   buffer->position = 0;
}

CFL_UINT32 cfl_buffer_capacity(CFL_BUFFERP buffer) {
   return buffer->capacity;
}

void cfl_buffer_setCapacity(CFL_BUFFERP buffer, CFL_UINT32 newCapacity) {
   if (newCapacity > 0) {
      if (newCapacity > buffer->capacity) {
         ensureCapacity(buffer, newCapacity);
      } else {
         buffer->data = realloc(buffer->data, newCapacity);
         buffer->capacity = newCapacity;
         if (buffer->length > newCapacity) {
            buffer->length = newCapacity;
            if (buffer->position > newCapacity) {
               buffer->position = newCapacity;
            }
         }
      }
   }
}

void cfl_buffer_putBoolean(CFL_BUFFERP buffer, CFL_BOOL value) {
   PUT_BUFFER(CFL_BOOL, buffer, value);
}

CFL_BOOL cfl_buffer_getBoolean(CFL_BUFFERP buffer) {
   RETURN_BUFFER(CFL_BOOL, buffer);  
}

void cfl_buffer_putInt8(CFL_BUFFERP buffer, CFL_INT8 value) {
   PUT_BUFFER(CFL_INT8, buffer, value);
}

CFL_INT8 cfl_buffer_getInt8(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(CFL_INT8, buffer);
}

void cfl_buffer_putInt16(CFL_BUFFERP buffer, CFL_INT16 value) {
   PUT_BUFFER(CFL_INT16, buffer, value);
}

CFL_INT16 cfl_buffer_getInt16(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(CFL_INT16, buffer);
}

void cfl_buffer_putInt32(CFL_BUFFERP buffer, CFL_INT32 value) {
   PUT_BUFFER(CFL_INT32, buffer, value);
}

CFL_INT32 cfl_buffer_getInt32(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(CFL_INT32, buffer);
}

void cfl_buffer_putInt64(CFL_BUFFERP buffer, CFL_INT64 value) {
   PUT_BUFFER(CFL_INT64, buffer, value);
}

CFL_INT64 cfl_buffer_getInt64(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(CFL_INT64, buffer);
}

void cfl_buffer_putUInt8(CFL_BUFFERP buffer, CFL_UINT8 value) {
   PUT_BUFFER(CFL_UINT8, buffer, value);
}

CFL_UINT8 cfl_buffer_getUInt8(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(CFL_UINT8, buffer);
}

void cfl_buffer_putUInt16(CFL_BUFFERP buffer, CFL_UINT16 value) {
   PUT_BUFFER(CFL_UINT16, buffer, value);
}

CFL_UINT16 cfl_buffer_getUInt16(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(CFL_UINT16, buffer);
}

void cfl_buffer_putUInt32(CFL_BUFFERP buffer, CFL_UINT32 value) {
   PUT_BUFFER(CFL_UINT32, buffer, value);
}

CFL_UINT32 cfl_buffer_getUInt32(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(CFL_UINT32, buffer);
}

void cfl_buffer_putUInt64(CFL_BUFFERP buffer, CFL_UINT64 value) {
   PUT_BUFFER(CFL_UINT64, buffer, value);
}

CFL_UINT64 cfl_buffer_getUInt64(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(CFL_UINT64, buffer);
}

void cfl_buffer_putFloat(CFL_BUFFERP buffer, float value) {
   PUT_BUFFER(float, buffer, value);
}

float cfl_buffer_getFloat(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(float, buffer);
}

void cfl_buffer_putDouble(CFL_BUFFERP buffer, double value) {
   PUT_BUFFER(double, buffer, value);
}

double cfl_buffer_getDouble(CFL_BUFFERP buffer) {
   RETURN_NUM_BUFFER(double, buffer);
}

CFL_STRP cfl_buffer_getString(CFL_BUFFERP buffer) {
   CFL_STRP str;
   CFL_UINT32 len;
   
   len = GET_NUM_BUFFER(CFL_UINT32, buffer);
   if (len > 0) {
      str = cfl_str_newBufferLen((char *)&buffer->data[buffer->position], len);
      buffer->position += len;
   } else {
      str = cfl_str_new(16);
   }
   return str;
}

CFL_UINT32 cfl_buffer_getStringLength(CFL_BUFFERP buffer) {
   //return *((CFL_UINT32 *)swapBytes(buffer, &buffer->data[buffer->position], sizeof(CFL_UINT32)));
   return *((CFL_UINT32 *)&buffer->data[buffer->position]);
}

void cfl_buffer_copyString(CFL_BUFFERP buffer, CFL_STRP destStr) {
   CFL_UINT32 len;
   
   len = GET_NUM_BUFFER(CFL_UINT32, buffer);
   if (len > 0) {
      cfl_str_setValueLen(destStr, (char *)&buffer->data[buffer->position], len);
      buffer->position += len;
   }
}

void cfl_buffer_copyStringLen(CFL_BUFFERP buffer, CFL_STRP destStr, CFL_UINT32 len) {
   CFL_UINT32 strLen;
   
   strLen = GET_BUFFER(CFL_UINT32, buffer);
   if (len > strLen) {
      len = strLen;
   }
   if (len > 0) {
      cfl_str_setValueLen(destStr, (char *)&buffer->data[buffer->position], len);
      buffer->position += len;
   }
}

void cfl_buffer_putString(CFL_BUFFERP buffer, CFL_STRP str) {
   CFL_UINT32 len = cfl_str_getLength(str);
   
   PUT_BUFFER(CFL_UINT32, buffer, len);
   if (len > 0) {
      ensureCapacity(buffer, buffer->length + len);
      memcpy((void *)&buffer->data[buffer->position], (void *)cfl_str_getPtr(str), len);
      buffer->position += len;
      if (buffer->position > buffer->length) {
         buffer->length = buffer->position;
      }
   }
}

void cfl_buffer_putStringLen(CFL_BUFFERP buffer, CFL_STRP str, CFL_UINT32 len) {
   CFL_UINT32 strLen;
   
   PUT_BUFFER(CFL_UINT32, buffer, len);
   if (len > 0) {
      strLen = cfl_str_getLength(str);
      if (strLen > len) {
         strLen = len;
      }
      ensureCapacity(buffer, buffer->length + len);
      memcpy((void *)&buffer->data[buffer->position], (void *)cfl_str_getPtr(str), strLen);
      while (strLen < len) {
         buffer->data[buffer->position + strLen] = ' ';
         ++strLen;
      }
      buffer->position += strLen;
      if (buffer->position > buffer->length) {
         buffer->length = buffer->position;
      }
   }
}

char * cfl_buffer_getCharArray(CFL_BUFFERP buffer) {
   char *str;
   CFL_UINT32 len;
   
   len = GET_NUM_BUFFER(CFL_UINT32, buffer);
   str = (char *) malloc((len + 1) * sizeof(char));
   if (len > 0) {
      memcpy((void *)str, (void *)&buffer->data[buffer->position], len * sizeof(char));
      buffer->position += len;
   }
   str[len] = 0;
   return str;
}

CFL_UINT32 cfl_buffer_getCharArrayLength(CFL_BUFFERP buffer) {
   //return *((CFL_UINT32 *)swapBytes(buffer, &buffer->data[buffer->position], sizeof(CFL_UINT32)));
   return *((CFL_UINT32 *)&buffer->data[buffer->position]);
}


void cfl_buffer_copyCharArray(CFL_BUFFERP buffer, char *destStr) {
   CFL_UINT32 len;
   
   len = GET_NUM_BUFFER(CFL_UINT32, buffer);
   if (len > 0) {
      memcpy((void *)destStr, (void *)&buffer->data[buffer->position], len * sizeof(char));
      buffer->position += len;
   }
   destStr[len] = 0;
}

void cfl_buffer_copyCharArrayLen(CFL_BUFFERP buffer, char *destStr, CFL_UINT32 len) {
   CFL_UINT32 maxBytes;
   CFL_UINT32 numBytes;
   
   numBytes = GET_BUFFER(CFL_UINT32, buffer);
   if (numBytes > 0) {
      maxBytes = len * sizeof(char);
      if (maxBytes > numBytes) {
         maxBytes = numBytes;
      }
      memcpy((void *)destStr, (void *)&buffer->data[buffer->position], maxBytes);
      buffer->position += maxBytes;
   }
}

void cfl_buffer_putCharArray(CFL_BUFFERP buffer, char *value) {
   CFL_UINT32 len = (CFL_UINT32) strlen(value) * sizeof(char);
   
   PUT_BUFFER(CFL_UINT32, buffer, len);
   if (len > 0) {
      ensureCapacity(buffer, buffer->length + len);
      memcpy((void *)&buffer->data[buffer->position], (void *)value, len);
      buffer->position += len;
      if (buffer->position > buffer->length) {
         buffer->length = buffer->position;
      }
   }
}

void cfl_buffer_putCharArrayLen(CFL_BUFFERP buffer, char *value, CFL_UINT32 len) {
   PUT_BUFFER(CFL_UINT32, buffer, len);
   if (len > 0) {
      ensureCapacity(buffer, buffer->length + len);
      memcpy((void *)&buffer->data[buffer->position], (void *)value, len * sizeof(char));
      buffer->position += len;
      if (buffer->position > buffer->length) {
         buffer->length = buffer->position;
      }
   }
}

void cfl_buffer_getDate(CFL_BUFFERP buffer, CFL_DATEP date) {
   date->year = (CFL_UINT16) GET_NUM_BUFFER(CFL_INT16, buffer);
   date->month = (CFL_UINT8) GET_NUM_BUFFER(CFL_INT8, buffer);
   date->day = (CFL_UINT8) GET_NUM_BUFFER(CFL_INT8, buffer);
}

void cfl_buffer_putDate(CFL_BUFFERP buffer, CFL_DATE value) {
   PUT_BUFFER(CFL_INT16, buffer, value.year);
   PUT_BUFFER(CFL_INT8, buffer, value.month);
   PUT_BUFFER(CFL_INT8, buffer, value.day);
}

void cfl_buffer_putDatePtr(CFL_BUFFERP buffer, CFL_DATEP value) {
   PUT_BUFFER(CFL_INT16, buffer, value->year);
   PUT_BUFFER(CFL_INT8, buffer, value->month);
   PUT_BUFFER(CFL_INT8, buffer, value->day);
}

CFL_UINT8 *cfl_buffer_get(CFL_BUFFERP buffer, CFL_UINT32 size) {
   void *res = malloc(size);
   
   memcpy(res, (CFL_UINT8 *)&buffer->data[buffer->position], size);
   buffer->position += size;
   return res;
}

void cfl_buffer_copy(CFL_BUFFERP buffer, CFL_UINT8 *dest, CFL_UINT32 size) {
   memcpy((CFL_UINT8 *)dest, (CFL_UINT8 *)&buffer->data[buffer->position], size);
   buffer->position += size;
}

void cfl_buffer_put(CFL_BUFFERP buffer, void *value, CFL_UINT32 size) {
   ensureCapacity(buffer, buffer->length + size);
   memcpy((void *)&buffer->data[buffer->position], (void *)value, size);
   buffer->position += size;
   if (buffer->position > buffer->length) {
      buffer->length = buffer->position;
   }
}

CFL_UINT32 cfl_buffer_remaining(CFL_BUFFERP buffer) {
   if (buffer->length > buffer->position) {
      return buffer->length - buffer->position;
   }
   return 0;
}

CFL_BOOL cfl_buffer_haveEnough(CFL_BUFFERP buffer, CFL_UINT32 need) {
   return buffer->length > buffer->position && need <= (buffer->length - buffer->position);
}

