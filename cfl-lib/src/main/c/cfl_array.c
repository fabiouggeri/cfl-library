#include <stdlib.h>  
#include <string.h>
#include "cfl_array.h"

CFL_ARRAYP cfl_array_new(CFL_UINT32 ulCapacity) {
   CFL_ARRAYP array;
   
   array = (CFL_ARRAYP) malloc(sizeof(CFL_ARRAY));
   array->objectType = CFL_TYPE_ARRAY;
   array->ulLength = 0;
   array->ulCapacity = ulCapacity;
   array->items = (void *)malloc(ulCapacity * sizeof(void *));
   return array;
}

CFL_ARRAYP cfl_array_newLen(CFL_UINT32 ulLen) {
   CFL_ARRAYP array;
   
   array = (CFL_ARRAYP) malloc(sizeof(CFL_ARRAY));
   array->objectType = CFL_TYPE_ARRAY;
   array->ulLength = ulLen;
   array->ulCapacity = ulLen;
   array->items = (void *)malloc(ulLen * sizeof(void *));
   return array;
}

void cfl_array_free(CFL_ARRAYP array) {
   if (array != NULL) {
      free(array->items);
      free(array);
   }
}

void cfl_array_add(CFL_ARRAYP array, void *item) {
   if (array->ulLength >= array->ulCapacity) {
      if ( array->ulCapacity > 0 ) {
         array->ulCapacity = ( array->ulCapacity >> 1 ) + 1 + array->ulLength;;
         array->items = (void *) realloc(array->items, array->ulCapacity * sizeof(void *));
      } else {
         array->ulCapacity = 12;
         array->items = (void *) malloc(array->ulCapacity * sizeof(void *));
      }
   }
   array->items[array->ulLength] = item;
   ++array->ulLength;
}

void cfl_array_del(CFL_ARRAYP array, CFL_UINT32 ulIndex) {
   if (ulIndex < array->ulLength) {
      CFL_UINT32 i;
      --array->ulLength;
      for (i = ulIndex; i < array->ulLength; i++) {
         array->items[i] = array->items[i + 1];
      }
      array->items[array->ulLength] = NULL;
   }
}

void *cfl_array_remove(CFL_ARRAYP array, CFL_UINT32 ulIndex) {
   if (ulIndex < array->ulLength) {
      CFL_UINT32 i;
      void *removed = array->items[ulIndex];
      --array->ulLength;
      for (i = ulIndex; i < array->ulLength; i++) {
         array->items[i] = array->items[i + 1];
      }
      array->items[array->ulLength] = NULL;
      return removed;
   }
   return NULL;
}

void *cfl_array_removeLast(CFL_ARRAYP array) {
   return cfl_array_remove(array, array->ulLength - 1);
}

void *cfl_array_get(CFL_ARRAYP array, CFL_UINT32 ulIndex) {
   if (ulIndex < array->ulLength) {
      return array->items[ ulIndex ];
   }
   return NULL;
}

void cfl_array_set(CFL_ARRAYP array, CFL_UINT32 ulIndex, void *item) {
   if (ulIndex < array->ulLength) {
      array->items[ ulIndex ] = item;
   }   
}

void cfl_array_clear(CFL_ARRAYP array) {
   if (array != NULL) {
      array->ulLength = 0;
   }
}

CFL_UINT32 cfl_array_length(CFL_ARRAYP array) {
   if (array != NULL) {
      return array->ulLength;
   }
   return 0;
}

void cfl_array_setLength(CFL_ARRAYP array, CFL_UINT32 newLen) {
   if (newLen < array->ulLength) {
      array->ulLength = newLen;
   } else if (newLen > array->ulLength) {
      if (newLen > array->ulCapacity) {
         array->ulCapacity = ( newLen >> 1 ) + 1 + newLen;;
         array->items = (void *) realloc(array->items, array->ulCapacity * sizeof(void *));
      }
      memset(&array->items[array->ulLength] ,0 , newLen - array->ulLength);
      array->ulLength = newLen;
   }
}

CFL_ARRAYP cfl_array_clone(CFL_ARRAYP other) {
   CFL_ARRAYP clone = cfl_array_newLen(other->ulLength);
   memcpy(clone->items, other->items, other->ulLength * sizeof(void *));
   return clone;   
}
